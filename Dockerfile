FROM  tensorflow/tensorflow:1.15.2-gpu-py3  

RUN apt update -y && apt upgrade -y  && apt install unzip nano git apt-utils wget htop -y  

RUN pip install bert-serving-server bert-serving-client jupyterlab

EXPOSE 5555 
EXPOSE 5556  

#RUN mkdir data  && cd data && wget https://storage.googleapis.com/bert_models/2019_05_30/wwm_cased_L-24_H-1024_A-16.zip && unzip wwm_cased_L-24_H-1024_A-16.zip
RUN mkdir -p /model  && cd /model && wget https://storage.googleapis.com/bert_models/2018_11_23/multi_cased_L-12_H-768_A-12.zip && unzip *.zip

RUN mkdir /data

COPY . /data

WORKDIR /data  

#CMD bert-serving-start -model_dir /model/multi_cased_L-12_H-768_A-12 -num_worker 4 -max_seq_len=NONE   
#CMD ["bert-serving-start", "-model_dir=/model/multi_cased_L-12_H-768_A-12", "-num_worker=4" "-max_seq_len=NONE"]  

CMD ["jupyter", "lab", "--allow-root", "--notebook-dir=/data/", "--ip=0.0.0.0" ]
